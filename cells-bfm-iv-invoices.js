{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-iv-invoices>` Description.

    Example:

    ```html
    <cells-bfm-iv-invoices></cells-bfm-iv-invoices>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-bfm-iv-invoices | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmIvInvoices extends Polymer.Element {

    static get is() {
      return 'cells-bfm-iv-invoices';
    }

    static get properties() {
      return {
        items: {
          type: Array,
          value: () => ([])
        }
      };
    }


  }

  customElements.define(CellsBfmIvInvoices.is, CellsBfmIvInvoices);
}